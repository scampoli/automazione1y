#include <stdio.h>
#include <string.h>
#include <ctype.h>
#define SPACE ' '
#define OPTIONS 2
#define MAXRIGA 100

void statistiche (FILE *);

int main (int argc, char * argv[])
{
	FILE *fin;

	if ((fin = fopen ("dizionario.txt", "r"))) {
		statistiche (fin);
	}
	
	return 0;
}

void statistiche (FILE *fin)
{
	int total, numCharRiga, maxCharRiga, righe, avg, i;
	char curChar, riga[MAXRIGA+1], maxRiga[MAXRIGA+1];

	total = 0;
	numCharRiga = 0;
	maxCharRiga = 0;
	righe = 1;
	i = 0;
	while (fscanf (fin, "%c", &curChar) != EOF) {
		riga[i] = curChar;
		i++;
		if (isgraph(curChar)) {
			total++;
			numCharRiga++;
		} else if (curChar == '\n') {
			if (numCharRiga > maxCharRiga) {
				maxCharRiga = numCharRiga;
				riga[i] = '\0';
				strcpy (maxRiga, riga);
			}
			numCharRiga = 0;
			i = 0;
			righe++;
		}
	}
	avg = total / righe;

	printf ("%d\n", righe);
	printf ("%d\n", total);
	printf ("%d\n", avg);
	printf ("%d\n", maxCharRiga);
	printf ("%s\n", maxRiga);

	return;
}
