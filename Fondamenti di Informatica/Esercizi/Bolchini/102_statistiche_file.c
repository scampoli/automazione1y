#include "list_char.h"
#include <ctype.h>
#define OPTIONS 2

int main (int argc, char * argv[])
{
	lista_t *head = NULL, *longest = NULL;
	char *inputfile;
	char curChar;
	int tot_char, tot_righe, char_line, max_char_line, avg_char_line;
	FILE *fin;

	if (argc == OPTIONS) {
		inputfile = argv[1];
		if ((fin = fopen(inputfile, "r"))) {
			tot_char = 0;
			tot_righe = 1;
			char_line = 0;
			max_char_line = 0;
			while (fscanf (fin, "%c", &curChar) != EOF) {
				head = append (head, curChar);
				if (isgraph(curChar)) {
					char_line++;
					tot_char++;
				} else if (curChar == '\n') {
					tot_righe++;
					if (char_line > max_char_line) {
						max_char_line = char_line;
						longest = empty (longest);
						longest = lstcpy (head, longest);
					}
					char_line = 0;
					head = empty (head);
				}
			}
			avg_char_line = tot_char / tot_righe;
			printf ("Ci sono %d righe\n", tot_righe);
			printf ("Ci sono %d caratteri (spazi esclusi)\n", tot_char);
			printf ("La riga piu' lunga e' di %d caratteri (spazi esclusi)\n", max_char_line);
			printf ("Le righe sono lunghe in media %d caratteri (spazi esclusi)\n", avg_char_line);
			printf (" Ecco la riga piu' lunga: ");
			printl (longest);
			longest = empty (longest);
			fclose (fin);
		} else {
			printf ("Errore nell'apertura del file %s\n", inputfile);
		}
	} else if (argc < OPTIONS) {
		printf ("Error: too few arguments\n");
	} else {
		printf ("Error: too many arguments\n");
	}

	return 0;
}