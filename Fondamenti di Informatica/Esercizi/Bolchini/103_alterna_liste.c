#include "list_int.h"
#define STOP 0

lista_t * alterna_liste (lista_t *, lista_t *);

int main (int argc, char * argv[])
{
	lista_t *l1 = NULL, *l2 = NULL, *ris;
	int val;
	
	scanf ("%d", &val);
	while (val != STOP) {
		l1 = append (l1, val);
		scanf ("%d", &val);
	}
	printf ("\n");
	scanf ("%d", &val);
	while (val != STOP) {
		l2 = append (l2, val);
		scanf ("%d", &val);
	}

	ris = alterna_liste (l1, l2);
	printl (ris);
	l1 = empty (l1);
	l2 = empty (l2);
	ris = empty (ris);

	return 0;
}

lista_t * alterna_liste (lista_t *l1, lista_t *l2)
{
	lista_t *ris = NULL;

	while (l1 || l2) {
		if (l1) {
			ris = append (ris, l1->val);
			l1 = l1->next;
		}
		if (l2) {
			ris = append (ris, l2->val);
			l2 = l2->next;
		}
	}

	return ris;
}
