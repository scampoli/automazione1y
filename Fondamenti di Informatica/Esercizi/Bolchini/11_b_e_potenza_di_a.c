/*Acquisire due valori interi positivi e finchè non sono tali li richiede, determini se b è una potenza di a e mostri l'esponente, altrimenti visualizza -1 */
#include <stdio.h>

int main(int argc, char * argv[])
{
	int a, b, count, ris, e;
	do {
		scanf("%d", &a);
	} while (a <= 0);
	do {
		scanf("%d", &b);
	} while (b <= 0);
	count = 1;
	/*if (a % b == 0) { Questa roba qua fa la stessa cosa ma per le divisioni anziché le potenze
		while (a * count != b) {
			count++;
		}
		ris = count;
	} else {
		ris = -1;
	} */
	if (a == 1 && b != 1) { /* serve se no quando entra nel ciclo va all'infinito */
		ris = -1; /* in realtà le eccezioni è meglio metterle nell'else */
	} else {
		e = 0; /* da qui count vuol dire potenza ed e esponente */
		while (count < b) {
			e++;
			count = count * a;
		}
		if (count == b) {
			ris = e;
		} else {
			ris = -1;
		}
	}
	printf("%d\n", ris);
	return 0;
}