#include <stdio.h>
#define NMAX 50
#define STOP 0

int isprime (int);

int main(int argc, char * argv[])
{
    int val[NMAX], i;
    int num, dim, senzaprimi;
    
    dim = 0;
    scanf("%d", &num);
    while(num > STOP){
        val[dim] = num;
        dim++;
        scanf("%d", &num);
    }
    senzaprimi = 1;
    for (i = 0; i < dim && senzaprimi == 1; i++) {
        if (isprime(val[i])) { /* Prende val[i] e lo salva nella variabile num che c'è sotto, non sporca la num qua sopra, volendo sarebbe la stessa cosa per isprimo se volessi riutilizzarla anche sopra */
			senzaprimi = 0;
		}
    }
    printf("%d\n", senzaprimi);
    return 0;
}

int isprime (int num /* non va ridichiarata sotto */)
{
	int isprimo, i, meta; /* Le variabili sono solo qua (scope (visibilità) limitato), si chiamano variabili locali e tipo i e num posso usarla sia sopra che sotto e non sovrascrivo niente di utile */
	
	if(num == 1 || num % 2 == 0 && num != 2)
		isprimo = 0;
	else {
		meta = num / 2;
		for(i = 3; i <= meta && isprimo == 1; i = i + 2) {
			if(num % i == 0) {
				isprimo = 0;
			}
		}
	}
	return isprimo;
}
