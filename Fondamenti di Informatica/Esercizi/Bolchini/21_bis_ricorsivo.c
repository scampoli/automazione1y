#include <stdio.h>

int fattoriale_r (int);

int main(int argc, char * argv[])
{
	int ris, val;

	scanf ("%d", &val);

	ris = fattoriale_r (val);

	printf ("%d\n", ris);

	return 0;
}

int fattoriale_r (int val)
{
	int fatt;

	if /* passo base, fine della ricorsione */ (val == 0 || val == 1) {
		fatt = 1;
	} else { /* passo induttivo */
		fatt = val * fattoriale_r (val-1);
	}
	
	return fatt;
}
