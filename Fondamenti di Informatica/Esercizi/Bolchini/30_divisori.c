#include <stdio.h>

int main(int argc, char * argv[])
{
	int x, y, resto, ris;
	scanf("%d", &x);
	scanf("%d", &y);
	if (x > y) {
		resto = x % y;
	} else {
		resto = y % x;
	}
	if (resto == 0) {
		ris = 1;
	} else {
		ris = 0;
	}
	printf("%d\n", ris);
	return 0;
}