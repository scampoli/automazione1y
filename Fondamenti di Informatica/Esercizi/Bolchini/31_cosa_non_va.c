#include <stdio.h>
#define FINE 20

int main (int argc, char * argv[])
{
	int min, max, val, cont, tot;
	float avg;
	scanf ("%d", &val);
	min = val;
	max = val;
	tot = val;
	cont = 1;
	scanf ("%d", &val);
	while (val != FINE) {
		if (val < min) {
			min = val;
		} else if (val > max) {
			max = val;
		}
		tot = tot + val;
		cont++;
		scanf ("%d", &val);
	}
	avg = (float) tot / cont;
	printf ("%d %d %f\n", min, max, avg);
	return 0;
}