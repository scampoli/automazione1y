#include <stdio.h>
#define DIM 4

int main(int argc, char * argv[])
{
	int mat[DIM][DIM];
	int i, j, valida;
	for (i = 0; i < DIM; i++) {
		for (j = 0; j < DIM; j++) {
			scanf ("%d", &mat[i][j]);
		}
	}
	valida = 1;
	for (i = 1; i < DIM && valida == 1; i++) {
		for (j = 1; j < DIM && valida == 1; j++) {
			if (i == j && mat[i][j] <= mat[i-1][j-1]) {
				valida = 0;
			}
		}
	}
	printf ("%d\n", valida);
	return 0;
}
