/* Acquisita una stringa di al più 25 caratteri, la visualizza al contrario */
#include <stdio.h>
#define LMAX 25

int main(int argc, char * argv[])
{
	char seq[LMAX+1];
	int dim;
	scanf("%s", seq);
	dim = 0;
	while (seq[dim] != '\0') {
		dim++;
	}
	for (dim--; dim >= 0; dim--) {
		printf("%c", seq[dim]);
	}
	printf("\n");
	return 0;
}
