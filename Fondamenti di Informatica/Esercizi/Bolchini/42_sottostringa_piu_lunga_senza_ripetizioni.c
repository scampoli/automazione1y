#include <stdio.h>
#include <string.h>
#define CMAX 30

int main(int argc, char * argv[])
{
	char input[CMAX+1];
	int ris, i, j, h, dim, len, check;

	scanf ("%s", input);
	dim = strlen (input);

	ris = 0;
	for (i = 0; i < dim - ris; i++) {
		check = 0;
		len = 0;
		for (h = i + 1; h <= dim && check == 0; h++) {
			len++;
			for (j = i; j < h && check == 0; j++) {
				if (input[h] == input[j]) {
					check = 1;
				}
			}
		}
		if (ris < len) {
			ris = len;
		}
	}

	printf("%d\n", ris);
	return 0;
}
