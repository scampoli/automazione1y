#include <stdio.h>
#define MAX 94

int main(int argc, char * argv[])
{
/* variabili */
	unsigned long long prev, last, next;
	int top, i, userinput;
/*acquisizione e print dei primi due valori*/
	printf("Inserisci quanti numeri della serie di Fibonacci vuoi stampare\n");
	scanf("%d", &userinput);
	if (userinput < MAX && userinput > 1) {
		top = userinput;
		printf("Ecco i primi %d valori della serie di Fibonacci\n", userinput);
	} else {
		top = MAX - 1;
		printf("Ti sto mostrando solo i primi 93 valori della serie di Fibonacci per evitare overflow o perche' hai inserito un numero minore di 2\n");
	}
	prev = 1;
	last = 1;
	printf("%d\n", prev);
	printf("%d\n", last);
/* elaborazione */
	for (i = 2; i < top; i++) {
		next = prev + last;
		printf("%llu\n", next);
		prev = last;
		last = next;
	}
	return 0;
}