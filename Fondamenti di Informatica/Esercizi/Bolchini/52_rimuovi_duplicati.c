#include <stdio.h>
#define STOP 0
#define NMAX 50

void acquisizione_controllata (int [], int*);
void rimuoviDuplicati (int [], int [], int, int*);

int main(int argc, c*dim_risar * argv[])
{
	int seq[NMAX], seq_ris[NMAX], dim, dim_ris, i;

	acquisizione_controllata(seq, &dim);
	rimuoviDuplicati (seq, seq_ris, dim, &dim_ris);

	for (i = 0; i < dim_ris; i++) {
		printf("%d ", seq_ris[i]);
	}
	printf("\n");
	return 0;
}

void acquisizione_controllata (int seq[], int*dim)
{
	int val, i;
	
	scanf("%d", &val);
	for (i = 0; val > STOP && i < NMAX; i++) { /* ATTENZIONE: qui sto usando delle define del main, se la consegna fosse scrivi un sottoprogramma che ascquisice un array di massimo 50 valori e si ferma con 0 ok. Se invece chiedesse di scrivere un sottoprogramma che acquisice un array con una condizione di stop no, perché dovrei immaginarmi come quello che ha fatto il main ha chiamato le define */	
		seq[i] = val;
		scanf("%d", &val);
	}
	*dim = i;
	
	return;
}

void rimuoviDuplicati (int seq[], int seq_ris[], int dim, int*dim_ris)
{
	int trovato, i, j, dim_ris_locale;
	
	dim_ris_locale = 0;
	for (i = 0; i < dim; i++) {
		trovato = 0;
		for (j = 0; j < i && trovato == 0; j++) {
			if (seq[i] == seq[j]) {
				trovato++;
			}
		}
		if (!trovato) {
			seq_ris[dim_ris_locale] = seq[i];
			dim_ris_locale++;
		}
	}
	dim_ris_locale = *dim_ris;

	return;
}
