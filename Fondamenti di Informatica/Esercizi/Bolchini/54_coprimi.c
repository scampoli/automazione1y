/* Restituisce 1 se due numeri sono primi tra loro, 0 altrimenti */
#include <stdio.h>

int coprimi (int, int);

int main(int argc, char * argv[])
{
	int a, b, ris;

	do {
		scanf("%d", &a);
	} while (a < 0);
	do {
		scanf("%d", &b);
	} while (b < 0);

	ris = coprimi(a, b);

	printf("%d\n", ris);
	return 0;
}

int coprimi (int a, int b)
{
	int ris, i, min;

	if (a == 1 || b == 1) {
		ris = 1;
	} else if (a == 0 || b == 0) {
		ris = 0;
	} else {
		if (a < b) {
			min = a;
		} else {
			min = b;
		}
		ris = 1;
		for (i = 2; i <= min && ris == 1; i++) {
			if (a % b == 0 && b % i == 0) {
				ris = 0;
			}
		}
	}

	return ris;
}
