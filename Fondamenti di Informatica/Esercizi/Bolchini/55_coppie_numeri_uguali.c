/* Dice quante coppie vengono in un array, ad esempio aba = 1; abba = 2; ... */
#include <stdio.h>
#define NMAX 100
#define STOP 0 /* si ferma se è uguale a zero */

int valida (int []);
void acquisizione_controllata (int [], int*);

int main(int argc, char * argv[])
{
	int seq[NMAX], dim, ris;

	acquisizione_controllata(seq, &dim);

	ris = valida(seq, dim);

	printf("%d\n", ris);
	return 0;
}

int valida (int seq [])
{
	int ris, i, j;

	ris = 0;
/* Questo in realtà vede solo se le coppie sono tipo aa bb, invece la richiesta è che aba venga ris = 1 */
/* 	for (i = 0; i < dim - 1; i += 2) {
		if (seq[i] == seq[i+1]) {
			ris++;
		}
	} */
	for (i = 0; i < dim - 1; i += 2) {
		for (j = i + 1; j < dim; j++) {
			if (seq[i] == seq[j]) {
				ris++;
			}
		}
	}

	return ris;
}

void acquisizione_controllata (int seq[], int*dim)
{
	int val, i;
	
	scanf("%d", &val);
	for (i = 0; i < NMAX && val != STOP; i++) {
		seq[i] = val;
		scanf("%d", &val);
	}
	*dim = i;
	
	return;
}
