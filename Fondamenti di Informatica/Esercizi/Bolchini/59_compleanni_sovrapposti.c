#include <stdio.h>
#define MAX 100

typedef struct date_s {
	int giorno, mese, anno;
} date_t;

void insieme (date_t [], date_t, int*, int*, int);

int main(int argc, char * argv[])
{
	date_t input[MAX], trg;
	int dim, i, compleanno_insieme, nati_insieme;

	printf("Quante date vuoi mettere?\n");
	do {
		scanf("%d", &dim);
	} while (dim <= 0);

	printf("Ora le date\n");
	for (i = 0; i < dim; i++) {
		do {
			scanf("%d", &input[i].giorno);
		} while (input[i].giorno <= 0);
		do {
			scanf("%d", &input[i].mese);
		} while (input[i].mese <= 0);
		scanf("%d", &input[i].anno);
	}

	printf("Ora la data da controllare\n");
	do {
		scanf("%d", &trg.giorno);
	} while (trg.giorno <= 0);
	do {
		scanf("%d", &trg.mese);
	} while (trg.mese <= 0);
	scanf("%d", &trg.anno);
	insieme(input, trg, &compleanno_insieme, &nati_insieme, dim);

	printf("%d %d\n", compleanno_insieme, nati_insieme);
	return 0;
}

void insieme (date_t input[], date_t trg, int*compleanno_insieme, int*nati_insieme, int dim)
{
	int i, comple, nati;
	comple = 0;
	nati = 0;

	for (i = 0; i < dim; i++) {
		if (input[i].giorno == trg.giorno && input[i].mese == trg.mese) {
			comple++;
			if (input[i].anno == trg.anno) {
				nati++;
			}
		}
	}
	*compleanno_insieme = comple;
	*nati_insieme = nati;

	return;
}
