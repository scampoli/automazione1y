/* Non è specificato cosa fare nei casi ambigui tipo "alla" quindi va bene tutto */
#include <stdio.h>
#define CMAX 100
#define FIRST_CHAR 'a'
#define LETTERS 26

int maxOccorrenze (char [], int*);

int main(int argc, char * argv[])
{
	char input[CMAX+1], ris;
	int ris_num, istr /* Il punto della stringa dove compare la lettera più frequente (Aggiunto dopo, per questo lo passo per riferimento senza passare così anche ris */;

	scanf("%s", input);
	
	ris_num = maxOccorrenze (input, &istr);
	ris = FIRST_CHAR /* Questa è una define fatta per il sottoprogramma, la sto usando solo perché non era la richiesta di stampare anche la lettera e quindo non l'ho messo sotto */+ ris_num - 1;

	printf ("%d %d %c\n", istr, ris_num, ris);
	return 0;
}

int maxOccorrenze (char input[] int*istr)
{
	int count[LETTERS], i, ris, dim, pos;

	dim = LETTERS;

	for (i = 0; i < dim; i++) {
		count[i] = 0;
	}

	pos = input[0] - FIRST_CHAR;
	count[pos]++;
	ris = pos;
	*istr = 0;

	for (i = 1; input[i] != '\0'; i++) {
		pos = input[i] - FIRST_CHAR;
		count[pos]++;
		if (count[pos] > count[ris]) {
			ris = pos;
			*istr = i;
		}
	}

	*istr++;
	ris++;

	return ris;
}
