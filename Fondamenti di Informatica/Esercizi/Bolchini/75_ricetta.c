#include <stdio.h>
#include <string.h>
#define CMAX 50
#define MAXINGREDIENTI 10
#define INPUT "./ricette.txt"
#define INPUT_DUE "./frigo.txt"
#define OUTPUT "posso_cucinare.txt"

typedef struct ingrediente_s {
	char nome[CMAX+1];
	float qta;
} ingrediente_t;

typedef struct ricetta_s {
	char nome[CMAX+1];
	int nPersone, nIngredienti;
	ingrediente_t ingredienti[MAXINGREDIENTI];
} ricetta_t;

int possoCucinare (ricetta_t, ingrediente_t [], int);

int main(int argc, char * argv[])
{
	ricetta_t ric;
	ingrediente_t dafrigo[MAXINGREDIENTI];
	FILE *fric, *ffrig, *fout;
	int i, count, numIngredienti, numRicetteFattibili;

	if (fric = fopen (INPUT, "r")) {
		if (ffrig = fopen (INPUT_DUE, "r")) {
			if (fout = fopen (OUTPUT, "w")) {
				/* Aquisisce dal file degli ingredienti */
				for (numIngredienti = 0; !feof (ffrig); numIngredienti++) {
					fscanf (ffrig, "%s", dafrigo[numIngredienti].nome);
					fscanf (ffrig, "%f", dafrigo[numIngredienti].qta);
				}
				numRicetteFattibili = 0;
				/* Aquisisce la ricetta */
				for (count = 0; !feof (fric); count++) {
					fscanf (fric, "%s", ric.nome);
					fscanf (fric, "%d", &ric.nPersone);
					fscanf (fric, "%d", &ric.nIngredienti);
					for (i = 0; i < ric.nIngredienti; i++) {
						fscanf (fric, "%s", ric.ingredienti[i].nome);
						fscanf (fric, "%f", &ric.ingredienti[i].qta);
					}
					/* Se posso cucinare scrivo sul file la ricetta */
					if (possoCucinare(ric, dafrigo, numIngredienti)) {
						fprintf (fout, "%s %d %d\n", ric.nome, ric.nPersone, ric.nIngredienti);
						for (i = 0; i < ric.nIngredienti; i++) {
							fprintf (fout, "%s %f\n", ric.ingredienti[i].nome, ric.ingredienti[i].qta);
						}
						numRicetteFattibili++;
					}
				}
				printf ("Le ricette totali sono %d, mentre quelle che puoi cucinare sono %d\n", count, numRicetteFattibili);
			} else {
				printf ("Problemi con il file %s\n", OUTPUT);
			}
			fclose (ffrig);
		} else {
			printf ("Problemi con il file %s\n", INPUT_DUE);
		}
		fclose (fric);
	} else {
		printf ("Problemi con il file %s\n", INPUT);
	}

	return 0;
}

int possoCucinare (ricetta_t ric, ingrediente_t ing[], int dim)
{
	int ris, trovato, i, j;

	if (dim >= ric.nIngredienti) { /* è solo un'ottimizzazione */
		ris = 1;
		for (i = 0; i < ric.nIngredienti && ris == 1; i++) {
			trovato = 0;
			for (j = 0; i < dim && trovato == 0; j++) {
				if (strcmp (ric.ingredienti[i].nome, ing[j].nome) && ric.ingredienti[i].qta <= ing[j].qta) {
					trovato = 1;
				}
			}
			ris = trovato;
		}
	} else {
		ris = 0;
	}
	
	return ris;
}

