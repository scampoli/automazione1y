#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define ALFA 26
#define CMAX 50

int main (int argc, char * argv[])
{
	char nome [CMAX+1], lettere [ALFA+1], curC;
	int *risultati;
	int i, dim;
	FILE *fin;

	scanf ("%s", lettere);
	scanf ("%s", nome);

	/* L'alternativa (meglio, perché accedere al file costa troppo) era scorrere il file una volta e per ogni lettera del file scorrere tutte le lettere dell'utente */
	/* Questo invece ha il while dentro e il for fuori, cioè scorre il file tutte le volte per ogni lettera dell'utente */
	/* La terza alternativa è contare tutte le lettere e poi stampare solo quelle necessarie, è la migliore in assoluto per CPU, ma usa più memoria */

	dim = strlen (lettere) - 1;
	if ((risultati = malloc (dim * sizeof (int)))) {
		if ((fin = fopen (nome, "r"))) {
			for (i = 0; i <= dim; i++) {
				risultati[i] = 0;
				while (fscanf (fin, "%c", &curC) != EOF) {
					if (curC == lettere[i]) {
						risultati[i]++;
					}
				}
				printf ("%c: %d ", lettere[i], risultati[i]);
				rewind (fin);
			}
			printf ("\n");
			fclose (fin);
		} else {
			printf ("Problemi con il file %s\n", nome);
		}
	} else {
		printf ("Errore allocazione memoria per %d interi\n", dim);
	}

	return 0;
}
