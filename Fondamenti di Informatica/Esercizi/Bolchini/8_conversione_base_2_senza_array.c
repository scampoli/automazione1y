#include <stdio.h>
#define BASE 2
#define MAXRAPP 2147483647

int main(int argc, char * argv[])
{
	int val, resto, potenzadi2;
	potenzadi2 = MAXRAPP;
	/*resto = 0;*/
	do {
		scanf("%d", &val);
	} while (val < 0);
	resto = val;
	/*while (potenzadi2 > 0) {
		if (resto + potenzadi2 <= val) {
			printf ("1");
			resto = resto + potenzadi2;
		} else if (resto != 0) {
			printf ("0");
		}
		potenzadi2 = potenzadi2 / BASE;
	}*/
	while (potenzadi2 > 0) {
		if (potenzadi2 <= resto) {
			printf ("1");
			resto = resto % potenzadi2;
		} else {
			printf ("0");
		}
		potenzadi2 = potenzadi2 / BASE;
	}
	printf("\n");
	return 0;
}
