#include <stdio.h>
#include <string.h>
#define LENMAXVERSO 40
#define NONUSATI "./futuribili.txt"
#define RIMAL 3
#define SPACE ' '
#define POINT '.'
#define SEMICOLON ';'
#define	LOINIT 'a'
#define	LOEND 'z'
#define	UPINIT 'A'
#define UPEND 'Z'
#define PERCENTUALE 0.2

typedef struct occ_s {
	int valore, cont;
} occ_t;

int lunghezzaComp (char [], char []);
int rima (char [], char []);
int rimaFile (char []);
int cesura (char []);
int cesuraFile (void);
int assonanza (char [], char []);
int assonanzaFile (char []);
int allitterazione (char [], occ_t []);
int allitterazioneFile (char);

int main(int argc, char * argv[])
{
    int scelta, continua, dim, i;
    char verso1[LENMAXVERSO + 1], verso2[LENMAXVERSO + 1], lettera;
	occ_t ris89[LENMAXVERSO];
	
	printf ("Buongiorno, qui ci sono le opzioni: https://polimi365-my.sharepoint.com/:b:/g/personal/10710677_polimi_it/Ef1gfUzLpjpCu-gF7RmJtmoBL8O8vhQ4Qd14NcZ7xrX1rg?e=8ZLq3M\n");
    continua = 1;
    do {
		scanf ("%d%*c", &scelta); /* Ignora il carattere terminatore in modo che non succedono casini quando ci sono in confronti, da usare nei menu */
        if (scelta == 0) {
            continua = 0;
        } else if (scelta == 1) {
			gets (verso1);
			gets (verso2);
			if (lunghezzaComp (verso1, verso2)) {
				printf ("I due versi sono compatibili\n");
			} else {
				printf ("I due versi non sono compatibili\n");
			}
        } else if (scelta == 2) {
			gets (verso1);
			gets (verso2);
			if (rima (verso1, verso2)) {
				printf("Sono in rima\n");
			} else {
				printf("Non sono in rima\n");
			}
        } else if (scelta == 3) {
            gets (verso1);
            if (!rimaFile (verso1)) {
				printf("Non ci sono versi in rima con quello inserito\n");
			}
        } else if (scelta == 4) {
            gets (verso1);
            if (!cesura (verso1)) {
                printf("Non ci sono punti o punti e virgola\n");
			}
        } else if (scelta == 5) {
            if (!(i = cesuraFile ())) {
                printf ("Non esiste una cesura\n");
			} else {
				printf ("Esistono %d versi su cui si puo' applicare la cesura\n", i);
			}
        } else if (scelta == 6) {
            gets (verso1);
			gets (verso2);
            if (assonanza (verso1, verso2)) {
                printf("%s, %s | Assonanza\n", verso1, verso2);
			} else {
                printf("%s, %s | Niente assonanza\n", verso1, verso2);
			}
        } else if( scelta == 7) {
            gets (verso1);
            if (!(i = assonanzaFile (verso1))) {
                printf ("Non esistono versi che fanno assonanza con quello inserito\n");
			} else {
				printf ("Esistono %d versi che fanno assonanza con quello inserito\n", i);
			}
        } else if (scelta == 8) {
            gets (verso1);
			if ((dim = allitterazione (verso1, ris89))) {
				for (i = 0; i < dim; i++) {
					printf ("%c fa allitterazione, infatti compare %d volte\n", (ris89[i].valore + LOINIT), ris89[i].cont);
				}
			} else {
				printf ("Non ci sono allitterazioni\n");
			}
        } else if (scelta == 9) {
            scanf ("%c", &lettera);
            if (!allitterazioneFile (lettera)) {
				printf ("Non ci sono versi che hanno %c come lettera che fa allitterazione\n", lettera);
			}
        } else {
            printf ("Input non valido\n");
		}
    } while(continua);

    return 0;
}

int lunghezzaComp (char verso1[], char verso2[]) {
	
	int comp, count1, count2;
	float tmp;
	
	count1 = strlen(verso1);
	count2 = strlen(verso2);
	
	if (count1 >= count2){
		tmp = (float) (count2 / count1);
	} else {
		tmp = (float) (count1 / count2);
	}
	
	comp = 0;
	if (tmp >= (1 - PERCENTUALE)) {
		comp = 1;
	}
	
	return comp;
}

int rima (char verso[], char controllo[])
{
	int dimV, dimC, i, ris;

	dimV = strlen(verso);
	dimC = strlen(controllo);

	ris = 1;
	for (i = 1; i < RIMAL + 1 && ris; i++) {
		ris = (verso[dimV - i] == controllo[dimC - i]);
	}

	return ris;
}

int rimaFile (char verso[])
{
	char controllo[LENMAXVERSO];
	int flag;
	FILE *fin;

	flag = 0;
	if ((fin = fopen (NONUSATI, "r"))) {
		while (!feof (fin)) {
			fgets (controllo, LENMAXVERSO, fin);
			if (rima(verso, controllo)) {
				printf ("Il verso inserito fa rima con <%s>\n", controllo);
				flag++;
			}
		}
		fclose(fin);
	} else {
		printf ("Errore nell'apertura del file %s\n", NONUSATI);
	}
	return flag;
}

int cesura (char verso[]) {
	int i, fine;
	char ris [LENMAXVERSO + 1];

	fine = 0;
	for (i = 0; verso[i] != '\0' && !fine; i++) {
		if (verso[i] == POINT || verso[i] == SEMICOLON) {
			fine = 1;
		}
	}

	if (fine) {
		memcpy (ris, verso, i); /* Copia la stringa verso nella strina ris fino a i */
		ris[i] = '\0';
		printf ("Il verso fino alla prima cesura e' <%s>\n", ris);
	}

	return fine;
}

int cesuraFile (void)
{
    FILE * fin;
    int found;
    char curS [LENMAXVERSO + 1];

    found = 0;
    if ((fin = fopen(NONUSATI, "r"))){
        while (!feof (fin)) {
			fgets (curS, LENMAXVERSO, fin);
            cesura(curS);
			found++;
		}
    } else {
        printf ("Errore nell'apertura del file %s\n", NONUSATI);
	}

    return found;
}

int assonanza(char s1 [], char s2 [])
{
    int i, voc1s1, voc2s1, voc1s2, voc2s2;

    voc1s1 = -1;
    voc2s1 = -1;
    for (i = 0; s1[i] != '\0'; i++){
        if (s1[i] == 'a' || s1[i] == 'e' || s1[i] == 'i' || s1[i] == 'o' || s1[i] == 'u' || s1[i] == 'A' || s1[i] == 'E' || s1[i] == 'I' || s1[i] == 'O' || s1[i] == 'U') {
            if (voc1s1 == -1) {
                voc1s1 = i;
            } else {
                voc2s1 = voc1s1;
                voc1s1 = i;
            }
        }
    }

	voc1s2 = -1;
    voc2s2 = -1;
    for (i = 0; s2[i] != '\0'; i++) {
        if (s2[i] == 'a' || s2[i] == 'e' || s2[i] == 'i' || s2[i] == 'o' || s2[i] == 'u' || s2[i] == 'A' || s2[i] == 'E' || s2[i] == 'I' || s2[i] == 'O' || s2[i] == 'U') {
            if (voc1s1 == -1) {
                voc1s2 = i;
            } else {
                voc2s2 = voc1s2;
                voc1s2 = i;
            }
        }
    }

	i = (voc1s1 == voc1s2) && (voc2s1 == voc2s2);

    return i;
}

int assonanzaFile(char verso[])
{
    FILE *fin;
    int found;
    char curS [LENMAXVERSO + 1];

    found = 0;
    if ((fin = fopen (NONUSATI, "r"))){
        while (!feof (fin)) {
			fgets (curS, LENMAXVERSO, fin);
            if (assonanza(verso, curS)) {
                printf("C'e' assonanza tra il verso inserito e <%s>\n", curS);
                found++;
            }
		}
    } else {
        printf ("Errore nell'apertura del file %s\n", NONUSATI);
	}

    return found;
}



int allitterazione (char verso[], occ_t ris[])
{
	char pulito[LENMAXVERSO + 1];
	int i, j, dim, trovato, numcontatori, pulito_int [LENMAXVERSO];
	occ_t numeri [LENMAXVERSO];
	float min_occorrenze;
	
	j = 0;
	for (i = 0; verso[i] != '\0'; i++) {
		while (verso[i] == SPACE || verso[i] == POINT || verso[i] == SEMICOLON) {
			i++;
		}
		pulito [j] = verso [i];
		j++;
	}
	pulito [j] = '\0';

	pulito_int [0] = 0;
	for (i = 0; pulito[i] != '\0'; i++) {
		if (pulito[i] >= LOINIT && pulito[i] <= LOEND) {
			pulito_int [i] = pulito [i] - LOINIT;
		} else {
			pulito_int [i] = pulito [i] - UPINIT;
		}
	}
	dim = i;

	min_occorrenze = (float) (dim * PERCENTUALE);
	numeri[0].valore = pulito_int[0];	/* il primo numero senz'altro non l'ho gia' visto */
	numeri[0].cont = 1;		/* l'ho visto 1 volta*/
	numcontatori = 1;	/* ho visto 1 valore di cui tenere il conteggio */
	for(i = 1; i < dim; i++) {
		/* cerco il valore che ho letto tra quelli di cui sto tenendo il conteggio */
		trovato = 0;
		j = 0;
		while (j < numcontatori && !trovato) {
			if (pulito_int[i] == numeri[j].valore) {
				trovato = 1;
			} else {
				j++;
			}
		}
		if (trovato) {	/* se l'ho trovato, j e' la sua posizione */
			numeri[j].cont++;						
		} else {
			/* numero mai visto prima, lo memorizzo e segno che l'ho visto 1 volta */
			numeri[numcontatori].valore = pulito_int[i];
			numeri[numcontatori].cont = 1;			
			numcontatori++;	/* c'e' un valore in piu' di cui tener traccia */
		}
	}

	j = 0;
	for (i = 0; i < numcontatori; i++) {
		if (numeri[i].cont >= min_occorrenze) {
			ris[j] = numeri[i];
			j++;
		}
	}

	return j;
}

int allitterazioneFile (char lettera)
{
	FILE *fin;
	int lettera_int, dim, i, errorLevel;
	char verso [LENMAXVERSO + 1];
	occ_t ris [LENMAXVERSO];
	
	errorLevel = 1;
	if ((fin = fopen (NONUSATI, "r"))) {
		if (lettera >= LOINIT && lettera <= LOEND) {
			lettera_int = lettera - LOINIT;
		} else {
			lettera_int = lettera - UPINIT;
		}
		while (!feof (fin)) {
			fgets (verso, LENMAXVERSO, fin);
			if ((dim = allitterazione (verso, ris))) {
				for (i = 0; i < dim; i++) {
					if (ris[i].valore == lettera_int) {
						printf ("In <%s> la lettera %c allittera %d volte\n", verso, lettera, ris[i].cont);
					}
				}
			} else {
				errorLevel = 0;
			}
		}
	} else {
		printf ("Errore nell'apertura del file %s\n", NONUSATI);
		errorLevel = 0;
	}
	
	return errorLevel;
}
