/* Programma che acquisice una sequenza di valori interi che si ritiene terminata quando l'utente inserisce 0, il programma visualizza i valori in ordine inverso */
/* Sottoprogramma che ricevuta in ingresso una lista, la restituisce dopo aver invertito l'ordine */
#include <stdio.h>
#include <stdlib.h>
#include "list_int.h"
#define STOP 0

lista_t * invertiLista (lista_t *);

int main (int argc, char * argv[])
{
	lista_t * head = NULL;
	int val;

	scanf ("%d", &val);
	while (val != STOP) {
		head = push (head, val); /* Uso push perché li dovrò stampare all'inverso */
		/* head = append (head, val);
		head = invertiLista (head); */
		scanf ("%d", &val);
	}

	printl (head);

	head = empty (head);
	
	return 0;
}

lista_t * invertiLista (lista_t *head)
{
	lista_t *lista = NULL;
	lista_t *tmp;

	tmp = head;
	while (tmp != NULL) {
		lista = push (lista, tmp->val);
		tmp = tmp->next;
	}

	head = empty (head);

	return lista;
}