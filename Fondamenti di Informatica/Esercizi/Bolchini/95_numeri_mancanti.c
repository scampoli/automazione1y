#include <stdio.h>
#include <stdlib.h>
#include "list_int.h"
#define COPPIA 2
#define CMAX 80

lista_t* numeriMancanti (char []);

int main (int argc, char * argv[])
{
	lista_t *head = NULL;
	char nome [CMAX+1];

	scanf ("%s", nome);

	head = numeriMancanti (nome);

	printl (head);

	head = empty (head);
	
	return 0;
}

lista_t* numeriMancanti (char nome[])
{
	int buf [COPPIA];
	lista_t *head = NULL;
	FILE *fin;

	if ((fin = fopen (nome, "r"))) {
		if (fscanf (fin, "%d", &buf[0]) != EOF) {
			while (fscanf (fin, "%d", &buf[1]) != EOF) {
				if ((buf[0] + 1) != buf[1]) {
					head = append (head, (buf[0] + 1));
				}
				buf[0] = buf[1];
			}
		} else {
			printf ("Il file %s e' vuoto\n", nome);
		}
	} else {
		printf ("Errore nell'apertura di %s\n", nome);
	}

	return head;
}